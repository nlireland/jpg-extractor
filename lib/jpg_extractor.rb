# frozen_string_literal: true

require 'logger'
require 'fileutils'
require 'json'
require 'yaml'
require 'mini_magick'
require 'uri'

APP_CONFIG = YAML.load_file(File.dirname(__FILE__) + '/../config/config.yml')

# Class to open and transform JP2 in the cache and write them to JPEG format.
class JPGExtractor
  def initialize(file = nil)
    setup_logger
    extract_images(parse(file))
    puts 'End'
  end

  private

  def load_all_images; end

  def parse(file)
    f = File.open(file)
    f.read.split
  rescue StandardError => e
    @logger.error(e)
    puts "Exiting because of error parsing file.\nCheck log for details."
    exit
  end

  def extract_images(list)
    list.each do |bib_id|
      path = find_path(bib_id)
      puts path
      image = MiniMagick::Image.open(
        "#{APP_CONFIG['jp2_derivative_path']}/#{path}/#{bib_id}_001.jp2"
      )
      image.format 'jpg'
      caption(image) if APP_CONFIG['caption']
      if APP_CONFIG['preserve_folders']
        FileUtils.mkdir_p "#{APP_CONFIG['output_path']}/#{path}"
        image.write "#{APP_CONFIG['output_path']}/#{path}/#{bib_id}_001.jpg"
      else
        FileUtils.mkdir_p "#{APP_CONFIG['output_path']}"
        image.write "#{APP_CONFIG['output_path']}/#{bib_id}_001.jpg"
      end
    rescue StandardError => e
      @logger.error("Error for: #{bib_id}")
      @logger.error(e)
    end
  end

  def caption(image)
        image.combine_options do |c|
        c.gravity 'Southwest'
        c.draw 'text 10,10 "National Library of Ireland / Leabharlann Náisiúnta na hÉireann"'
        c.font '-*-helvetica-*-r-*-*-18-*-*-*-*-*-*-2'
        c.fill("#FFFFFF")
      end
      image
  end

  def find_path(bib_id)
    group = bib_id.gsub(/vtls/, '')
    padded_round = group.to_i.ceil(-4).to_s.rjust(9, '0')
    "#{padded_round}/#{group}"
  end

  def setup_logger
    log_path = APP_CONFIG['log_path']
    logger_path = File.join(log_path, "parser_log-#{DateTime.now.strftime('%d-%m-%Y_%H-%M-%S')}.log")
    FileUtils.touch(logger_path) unless File.file? logger_path
    @logger = Logger.new logger_path
  end

  def measure(&block)
    start = Time.now
    block.call
    puts "#{block.inspect} - took #{Time.now - start}\n"
  end
end
