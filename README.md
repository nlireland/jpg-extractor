# README #

Simple script to copy jp2 files from the NLI image derivatives folder and convert them to jpg format. Uses ImageMagick and mini_magic gem. All bib_ids are assumed to have only one image e.g. <vtls>_001.jp2. 

### Install ###

Just git pull the repository, and copy config/config.yml.EXAMPLE to non example file and update the values in them according to your machine setup.

#### To install on Linux ####

###### Install Ruby ######

```
yum install ruby
yum install ruby-devel
```

###### Install Git ######
```
yum install git
git config --global user.name "Your Actual Name"
git config --global user.email "Your Actual Email"
```

###### Folder Setup #######
```
git clone https://bitbucket.org/nlireland/jpg_extractor
cd ./jpg_extractor
```

###### Ruby Gems Bundle #######
```
bundle install
```

- set config/config.yml.EXAMPLE

### Running the script ###
The script expects to be passed the relative path to a space seperated list of vtls numbers e.g.
```
vtls000555980 vtls000556521 vtls000559376 vtls000559459
```

* To run the script, do `ruby bin/extract.rb <path_to_bib_id_list>`
