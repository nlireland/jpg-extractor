#!/usr/bin/env ruby
# frozen_string_literal: true

require 'fileutils'
require "#{__dir__}/../lib/jpg_extractor.rb"

puts 'started'
if $PROGRAM_NAME == __FILE__
  # do things
  ARGV[0]
  JPGExtractor.new ARGV[0]
  puts 'extracting ok'
  puts 'extractor finished'
end
puts 'finished'
